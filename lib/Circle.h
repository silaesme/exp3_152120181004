/*
 * Circle.h
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#ifndef CIRCLE_H_
#define CIRCLE_H_
namespace Student1 {
	class Circle {
	public:
		Circle(double);
		virtual ~Circle();
		void setR(double);
		void setR(int);
		double getR();
		double getR() const;
		double calculateCircumference();
		double calculateCircumference()const;
		double calculateArea();
		double calculateArea()const;
		bool isEqual(Circle x);
	private:
		double r;
		const double PI = 3.14;
		bool setctrl = 0;
	};
}
#endif /* CIRCLE_H_ */
