/*
 * Circle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */


#include "Circle.h"
#include <iostream>
namespace Student1 {
Circle::Circle(double r) {
	//for question 4, PI cannot changed. line below has compile error
	/*PI = 3.15;*/
	setR(r);
}

Circle::~Circle() {
}

void Circle::setR(double r){
	this->r = r;
	this->setctrl = 1;
}
void Circle::setR(int r)
{
	this->r = r;
}

double Circle::getR(){
	return r;
}
double Circle::getR()const{
	return r;
}

double Circle::calculateCircumference(){
	return PI * 2 * r;
}
double Circle::calculateCircumference()const{
	return PI * 2 * r;
}

double Circle::calculateArea(){
	return PI * r * r;
}
double Circle::calculateArea()const{
	return PI * r * r;
}
bool Circle::isEqual(Circle x) {
	if (x.getR() == r)
	{
		return true;
	}
	return false;
}
}
